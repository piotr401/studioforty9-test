<?php

namespace App\Repository;

use App\Entity\Stock;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Stock|null find($id, $lockMode = null, $lockVersion = null)
 * @method Stock|null findOneBy(array $criteria, array $orderBy = null)
 * @method Stock[]    findAll()
 * @method Stock[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class StockRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Stock::class);
    }

    public function findOneToEdit($id) {
        $qb = $this->createQueryBuilder('s')
            ->select('s', 'p', 'b')
	        ->leftJoin('s.product', 'p')
	        ->leftJoin('s.branch', 'b')
            ->andWhere('s.id =:id')
            ->setParameter('id', $id)
        ;
        return $qb->getQuery()->getOneOrNullResult();
    }

	public function findOneBySkuAndBranch($sku, $branch) {
		$qb = $this->createQueryBuilder('s')
			->select('s')
			->leftJoin('s.product', 'p')
			->leftJoin('s.branch', 'b')
			->andWhere('p.sku =:sku')
			->setParameter('sku', $sku)
			->andWhere('b.name =:branch')
			->setParameter('branch', $branch)
		;
		return $qb->getQuery()->getOneOrNullResult();
	}

	public function findAmountSum($id) {
		$qb = $this->createQueryBuilder('s')
			->select('SUM(a.value) as amount')
			->leftJoin('s.amounts', 'a')
			->andWhere('s.id =:id')
			->setParameter('id', $id)
		;
		return $qb->getQuery()->getSingleScalarResult();
	}
}
