<?php

namespace App\Repository;

use App\Entity\Branch;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Branch|null find($id, $lockMode = null, $lockVersion = null)
 * @method Branch|null findOneBy(array $criteria, array $orderBy = null)
 * @method Branch[]    findAll()
 * @method Branch[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class BranchRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Branch::class);
    }

	public function findAllQuery() {
		$qb = $this->createQueryBuilder('b')
			->select('b')
		;
		return $qb->getQuery();
	}

	public function findOneToEdit($id) {
		$qb = $this->createQueryBuilder('b')
			->select('b')
			->andWhere('b.id =:id')
			->setParameter('id', $id)
		;
		return $qb->getQuery()->getOneOrNullResult();
	}

	public function findOneByName($name) {
		$qb = $this->createQueryBuilder('b')
			->select('b')
			->andWhere('b.name =:name')
			->setParameter('name', $name)
		;
		return $qb->getQuery()->getOneOrNullResult();
	}
}
