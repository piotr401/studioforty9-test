<?php

namespace App\Repository;

use App\Entity\Product;
use Doctrine\Bundle\DoctrineBundle\Repository\ServiceEntityRepository;
use Doctrine\Persistence\ManagerRegistry;

/**
 * @method Product|null find($id, $lockMode = null, $lockVersion = null)
 * @method Product|null findOneBy(array $criteria, array $orderBy = null)
 * @method Product[]    findAll()
 * @method Product[]    findBy(array $criteria, array $orderBy = null, $limit = null, $offset = null)
 */
class ProductRepository extends ServiceEntityRepository
{
    public function __construct(ManagerRegistry $registry)
    {
        parent::__construct($registry, Product::class);
    }

	public function findAllQuery() {
		$qb = $this->createQueryBuilder('p')
			->select('p')
		;
		return $qb->getQuery();
	}

	public function findOneToEdit($id) {
		$qb = $this->createQueryBuilder('p')
			->select('p')
			->andWhere('p.id =:id')
			->setParameter('id', $id)
		;
		return $qb->getQuery()->getOneOrNullResult();
	}

	public function findOneBySku($sku) {
		$qb = $this->createQueryBuilder('p')
			->select('p')
			->andWhere('p.sku =:sku')
			->setParameter('sku', $sku)
		;
		return $qb->getQuery()->getOneOrNullResult();
	}
}
