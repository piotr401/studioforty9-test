<?php

namespace App\tests\Controller;

use App\Repository\BranchRepository;
use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HTTPFoundation\Response;

class BranchControllerTest extends WebTestCase
{
	private KernelBrowser|null $client = null;
	private $urlGenerator;

	public function setUp() : void
	{
		$this->client = static::createClient();
		$this->urlGenerator = $this->client->getContainer()->get('router.default');
	}

	public function testBranchListingIsUp()
	{
		$crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('branch_list'));
		$this->assertResponseStatusCodeSame(Response::HTTP_OK);
	}

	public function testBranchCreation()
	{
		$crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('branch_create'));
		$this->assertResponseStatusCodeSame(Response::HTTP_OK);

		$buttonCrawlerNode = $crawler->selectButton('Submit');
		$form = $buttonCrawlerNode->form();
		$form['branch[name]'] = 'WEB';
		$this->client->submit($form);

		$crawler = $this->client->followRedirect();

		$this->assertSame(
			1,
			$crawler->filter('html:contains("Edit Branch")')->count()
		);
	}

	public function testBranchEdition()
	{
		$branchRepository = static::getContainer()->get(BranchRepository::class);
		$testBranch = $branchRepository->findOneByName('WPI');

		$crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('branch_edit', array(
			'id'    => $testBranch->getId()
		)));
		$this->assertResponseStatusCodeSame(Response::HTTP_OK);

		$buttonCrawlerNode = $crawler->selectButton('Submit');
		$form = $buttonCrawlerNode->form();

		//dump($form['branch[name]']->getValue()); die;

		$this->client->submit($form);

		$crawler = $this->client->followRedirect();

		$this->assertResponseStatusCodeSame(Response::HTTP_OK);

		$this->assertSame(
			1,
			$crawler->filter('html:contains("Edit Branch")')->count()
		);
	}

	public function testBranchDeleting()
	{
		$crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('branch_list'));
		$this->assertResponseStatusCodeSame(Response::HTTP_OK);

		$link = $crawler->selectLink('Delete')->link();
		$this->client->click($link);

		$crawler = $this->client->followRedirect();

		$this->assertResponseStatusCodeSame(Response::HTTP_OK);
	}
}