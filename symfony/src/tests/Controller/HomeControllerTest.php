<?php

namespace App\tests\Controller;

use Symfony\Bundle\FrameworkBundle\KernelBrowser;
use Symfony\Bundle\FrameworkBundle\Test\WebTestCase;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HTTPFoundation\Response;

class HomeControllerTest extends WebTestCase
{
	private KernelBrowser|null $client = null;
	private $urlGenerator;

	public function setUp() : void
	{
		$this->client = static::createClient();
		$this->urlGenerator = $this->client->getContainer()->get('router.default');
	}

	public function testHomepageIsUp()
	{


		$crawler = $this->client->request(Request::METHOD_GET, $this->urlGenerator->generate('dashboard'));

		$this->assertResponseStatusCodeSame(Response::HTTP_OK);
	}
}