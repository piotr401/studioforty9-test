<?php

namespace App\tests\Form;

use App\Entity\Branch;
use App\Form\Type\BranchType;
use Symfony\Component\Form\Test\TypeTestCase;

class BranchFormTest extends TypeTestCase
{
	public function testSubmitValidData()
	{
		$formData = [
			'name' => 'WAR'
		];

		$model = new Branch();

		$form = $this->factory->create(BranchType::class, $model);

		$expected = new Branch();
		$expected->setName($formData['name']);

		$form->submit($formData);

		$this->assertTrue($form->isSynchronized());

		$this->assertEquals($expected->getName(), $model->getName());
	}
}