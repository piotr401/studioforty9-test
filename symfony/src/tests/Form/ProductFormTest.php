<?php

namespace App\tests\Form;

use App\Entity\Amount;
use App\Entity\Branch;
use App\Entity\Product;
use App\Entity\Stock;
use App\Form\Type\ProductType;
use Doctrine\ORM\AbstractQuery;
use Doctrine\ORM\EntityManager;
use Doctrine\ORM\EntityRepository;
use Doctrine\ORM\Mapping\ClassMetadata;
use Doctrine\ORM\QueryBuilder;
use Doctrine\Persistence\ManagerRegistry;
use Symfony\Bridge\Doctrine\Form\DoctrineOrmExtension;
use Symfony\Component\Form\Test\TypeTestCase;

class ProductFormTest extends TypeTestCase
{
	protected function getExtensions() {

		$mockEntityManager = $this->createMock(EntityManager::class);
		$mockEntityManager->method('getClassMetadata')
			->willReturn(new ClassMetadata(Product::class))
		;

		$execute = $this->createMock(AbstractQuery::class);
		$execute->method('execute')
			->willReturn([]);

		$query = $this->createMock(QueryBuilder::class);
		$query->method('getQuery')
			->willReturn($execute);


		$entityRepository = $this->createMock(EntityRepository::class);
		$entityRepository->method('createQueryBuilder')
			->willReturn($query)
		;

		$mockEntityManager->method('getRepository')->willReturn($entityRepository);

		$mockRegistry = $this->createMock(ManagerRegistry::class);
		$mockRegistry->method('getManagerForClass')
			->willReturn($mockEntityManager)
		;

		return array_merge(parent::getExtensions(), [new DoctrineOrmExtension($mockRegistry)]);
	}

	public function testSubmitValidData()
	{
		$formData = [
			'sku' => '000000000000099'
		];

		$model = new Product();
		$form = $this->factory->create(ProductType::class, $model);

		$expected = new Product();
		$expected->setSku($formData['sku']);

		$form->submit($formData);

		$this->assertTrue($form->isSynchronized());
		$this->assertEquals($expected->getSku(), $model->getSku());
	}
}