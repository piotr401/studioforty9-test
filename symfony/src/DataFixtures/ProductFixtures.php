<?php

namespace App\DataFixtures;

use App\Entity\Amount;
use App\Entity\Branch;
use App\Entity\Product;
use App\Entity\Stock;
use Doctrine\Bundle\FixturesBundle\Fixture;
use Doctrine\Persistence\ObjectManager;

class ProductFixtures extends Fixture
{
    public function load(ObjectManager $manager): void
    {
		$skus = [
			"0000000000001",
			"0000000000002",
			"0000000000003",
			"0000000000004",
			"0000000000005",
			"0000000000006",
			"0000000000007",
			"0000000000008",
			"0000000000009",
			"0000000000010"
		];

		$branch = new Branch();
		$branch->setName('WPI');

        $products = $this->createProducts($skus);


		$i = -2;
		foreach ($products as $product) {

			$amount = new Amount($i);

			$stock = new Stock();
			$stock->setBranch($branch);
			$stock->setProduct($product);
			$stock->addAmount($amount);

			if ($i <= 0) {
				$stock->setStatus(0);
			} else {
				$stock->setStatus(1);
			}

			$manager->persist($stock);

			$i++;
		}

        $manager->flush();
    }

	public function createProducts($skus) {
		$results = [];

		foreach ($skus as $sku) {
			$product = new Product();
			$product->setSku($sku);
			$results[] = $product;
		}
		return $results;
	}
}
