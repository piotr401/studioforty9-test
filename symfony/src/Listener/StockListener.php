<?php

namespace App\Listener;

use App\Entity\Amount;
use App\Entity\Stock;
use App\Message\OutOfStockAlert;
use App\Utils\Managers\StockManager;
use Doctrine\ORM\EntityManagerInterface;
use Doctrine\ORM\Event\LifecycleEventArgs;
use Doctrine\ORM\Event\OnFlushEventArgs;
use Doctrine\ORM\Event\PostFlushEventArgs;
use Doctrine\ORM\Mapping\PostLoad;
use Symfony\Component\Messenger\MessageBusInterface;

class StockListener
{
    protected $em;
	protected $bus;
	protected $stockManager;

    public function __construct(EntityManagerInterface $em, MessageBusInterface $bus, StockManager $stockManager)
    {
        $this->em = $em;
		$this->bus = $bus;
		$this->stockManager = $stockManager;
    }

	public function prePersist(LifecycleEventArgs $args)
	{
		$entity = $args->getObject();

		if ($entity instanceof Stock) {

			if ($entity->getAmount()) {
				$amount = new Amount($entity->getAmount());
				$entity->addAmount($amount);
				$entity = $this->stockManager->checkAmount($entity);

				if ($entity->getTotal() > 0 && ($entity->getTotal() + $entity->getAmount()) <= 0) {
					$this->bus->dispatch(new OutOfStockAlert($entity->getId()));
				}
			}
		}
	}

	public function preUpdate(LifecycleEventArgs $args)
	{
		$this->prePersist($args);
	}

    public function postLoad(LifecycleEventArgs $args)
    {
        $entity = $args->getObject();
        if ($entity instanceof Stock) {
	        $entity = $this->stockManager->checkAmount($entity);
        }
    }
}
