<?php

namespace App\Message;

class OutOfStockAlert
{
	private $stockId;

	public function __construct(int $stockId)
	{
		$this->stockId = $stockId;
	}

	public function getStockId(): int
	{
		return $this->stockId;
	}
}
