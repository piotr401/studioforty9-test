<?php
/**
 * Created by PhpStorm.
 * User: piotr
 * Date: 26/12/2017
 * Time: 18:33
 */

namespace App\Utils\Services;

use App\Entity\Cart;
use App\Entity\Discount;
use App\Entity\Newsletter;
use App\Entity\Order;
use App\Entity\RecoverRequest;
use App\Entity\Stock;
use App\Entity\User;
use App\Utils\Managers\ControlManager;
use PharIo\Manifest\InvalidEmailException;
use Symfony\Component\Mailer\MailerInterface;
use Symfony\Component\Mime\Address;
use Symfony\Component\Mime\Email;
use Symfony\Component\Routing\Generator\UrlGeneratorInterface;
use Twig\Environment;

/**
 * Class CoreServices
 * @package App\Services
 */
class MailerService
{
    /** @var MailerInterface  */
    private $mailer;

    private $from;
	private $to;

    /**
     * MailerService constructor.
     *
     * @param MailerInterface       $mailer
     */
    public function __construct(MailerInterface $mailer, $from, $to)
    {
        $this->mailer = $mailer;
        $this->from = $from;
	    $this->to = $to;
    }

    public function outOfStockAlertEmail(Stock $stock) {

		$productSku = $stock->getProduct()->getSku();
		$branchName = $stock->getBranch()->getName();

		$content = "The product with SKU ".$productSku." for branch ".$branchName." is out of stock";
		$subject = "Product out of stock";

        $this->sendEmail($content, $subject);
    }

    public function sendEmail($content, $subject)
    {
	    $email = (new Email())
		    ->from(Address::create('Studio <' . $this->from . '>'))
		    ->to($this->to)
		    ->subject($subject)
		    ->text($content);

	    $this->mailer->send($email);
	}
}
