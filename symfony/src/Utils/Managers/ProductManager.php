<?php

namespace App\Utils\Managers;

use App\Entity\Product;
use App\Repository\ProductRepository;
use Doctrine\ORM\EntityManagerInterface;

class ProductManager
{
    private $em;

    private $productRepository;

    public function __construct(EntityManagerInterface $em, ProductRepository $productRepository)
    {
        $this->em = $em;
        $this->productRepository = $productRepository;
    }


    /**
     * Save product
     *
     * @param Product $product
     */
    public function save(Product $product)
    {
        $this->em->persist($product);
        $this->em->flush();

        return $product;
    }

    /**
     * @param Product $product
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Product $product)
    {
        $this->em->remove($product);
        $this->em->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->productRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->productRepository->findOneBy($filters);
    }

	/**
	 * Find one to edit by id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function findOneToEdit(int $id)
	{
		return $this->productRepository->findOneToEdit($id);
	}

	/**
	 * Find all query
	 *
	 * @return mixed
	 */
	public function findAllQuery()
	{
		return $this->productRepository->findAllQuery();
	}

	/**
	 * Find one by sku
	 *
	 * @param string $sku
	 * @return mixed
	 */
	public function findOneBySku($sku)
	{
		return $this->productRepository->findOneBySku($sku);
	}

	public function checkStockDuplicate(Product $product) {
		$names = [];
		$duplicates = null;
		foreach ($product->getStocks() as $stock) {

			if (!in_array($stock->getBranch()->getName(), $names)) {
				$names[] = $stock->getBranch()->getName();
			} else {
				$duplicates[] = $stock->getBranch()->getName();
			}
		}

		return $duplicates;
	}
}
