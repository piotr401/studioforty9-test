<?php

namespace App\Utils\Managers;

use App\Entity\Branch;
use App\Repository\BranchRepository;
use Doctrine\ORM\EntityManagerInterface;

class BranchManager
{
    private $em;

    private $branchRepository;

    public function __construct(EntityManagerInterface $em, BranchRepository $branchRepository)
    {
        $this->em = $em;
        $this->branchRepository = $branchRepository;
    }


    /**
     * Save branch
     *
     * @param Branch $branch
     */
    public function save(Branch $branch)
    {
        $this->em->persist($branch);
        $this->em->flush();

        return $branch;
    }

    /**
     * @param Branch $branch
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Branch $branch)
    {
        $this->em->remove($branch);
        $this->em->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->branchRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->branchRepository->findOneBy($filters);
    }

	/**
	 * Find one to edit by id
	 *
	 * @param int $id
	 * @return mixed
	 */
	public function findOneToEdit(int $id)
	{
		return $this->branchRepository->findOneToEdit($id);
	}

	/**
	 * Find all query
	 *
	 * @return mixed
	 */
	public function findAllQuery()
	{
		return $this->branchRepository->findAllQuery();
	}

	/**
	 * Find one by name
	 *
	 * @param string $name
	 * @return mixed
	 */
	public function findOneByName($name)
	{
		return $this->branchRepository->findOneByName($name);
	}
}
