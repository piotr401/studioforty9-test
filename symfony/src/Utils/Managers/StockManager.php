<?php

namespace App\Utils\Managers;

use App\Entity\Stock;
use App\Repository\StockRepository;
use Doctrine\ORM\EntityManagerInterface;

class StockManager
{
    private $em;

    private $stockRepository;

    public function __construct(EntityManagerInterface $em, StockRepository $stockRepository)
    {
        $this->em = $em;
        $this->stockRepository = $stockRepository;
    }


    /**
     * Save stock
     *
     * @param Stock $stock
     */
    public function save(Stock $stock)
    {
        $this->em->persist($stock);
        $this->em->flush();

        return $stock;
    }

    /**
     * @param Stock $stock
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Stock $stock)
    {
        $this->em->remove($stock);
        $this->em->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->stockRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->stockRepository->findOneBy($filters);
    }

	/**
	 * Find one to edit by id
	 *
	 * @param int $id
	 * @return mixed
	 */
    public function findOneToEdit(int $id)
    {
        return $this->stockRepository->findOneToEdit($id);
    }

	/**
	 * Find one by sku and branch
	 *
	 * @param string $sku
	 * @param string $branch
	 * @return mixed
	 */
	public function findOneBySkuAndBranch(String $sku, String $branch) {
		return $this->stockRepository->findOneBySkuAndBranch($sku, $branch);
	}

	public function checkAmount(Stock $stock) {
		$sum = $this->stockRepository->findAmountSum($stock->getId());
		if ($sum <= 0) {
			$stock->setStatus(0);
		} else {
			$stock->setStatus(1);
		}
		$stock->setTotal($sum);
		return $stock;
	}
}
