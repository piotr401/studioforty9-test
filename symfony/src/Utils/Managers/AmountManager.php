<?php

namespace App\Utils\Managers;

use App\Entity\Amount;
use App\Repository\AmountRepository;
use Doctrine\ORM\EntityManagerInterface;
use phpDocumentor\Reflection\Types\Integer;

class AmountManager
{
    private $em;

    private $amountRepository;

    public function __construct(EntityManagerInterface $em, AmountRepository $amountRepository)
    {
        $this->em = $em;
        $this->amountRepository = $amountRepository;
    }


    /**
     * Save amount
     *
     * @param Amount $amount
     */
    public function save(Amount $amount)
    {
        $this->em->persist($amount);
        $this->em->flush();

        return $amount;
    }

    /**
     * @param Amount $amount
     *
     * @throws \Doctrine\ORM\ORMException
     * @throws \Doctrine\ORM\OptimisticLockException
     */
    public function remove(Amount $amount)
    {
        $this->em->remove($amount);
        $this->em->flush();
    }

    /**
     * Find all
     *
     * @return mixed
     */
    public function findAll()
    {
        return $this->amountRepository->findAll();
    }

    /**
     * Find one by
     *
     * @param array $filters
     * @return mixed
     */
    public function findOneBy($filters = array())
    {
        return $this->amountRepository->findOneBy($filters);
    }
}
