<?php

namespace App\Entity;

use Doctrine\ORM\Mapping as ORM;
use phpDocumentor\Reflection\Types\Integer;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Amount
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * Status
	 *
	 * @ORM\Column(type="integer", nullable=false)
	 */
	protected $value;

	/**
	 * Stock
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Stock", inversedBy="amounts", cascade={"persist"})
	 * @ORM\JoinColumn(name="stock_id", referencedColumnName="id", onDelete="SET NULL")
	 * @var Stock|null
	 */
	protected $stock;

	/**
	 * Creation date
	 *
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	public function __construct($value)
	{
		$this->value = $value;
		$this->created = new \DateTime();
	}

    /**
     * Set id
     *
     * @param integer $id
     * @return Amount
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return mixed
	 */
	public function getValue()
	{
		return $this->value;
	}

	/**
	 * @param mixed $value
	 */
	public function setValue($value): void
	{
		$this->value = $value;
	}

	/**
	 * @return Stock|null
	 */
	public function getStock(): ?Stock
	{
		return $this->stock;
	}

	/**
	 * @param Stock|null $stock
	 */
	public function setStock(?Stock $stock): void
	{
		$this->stock = $stock;
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created): void
	{
		$this->created = $created;
	}
}
