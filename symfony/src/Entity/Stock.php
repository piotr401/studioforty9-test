<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\StockRepository")
 * @ORM\HasLifecycleCallbacks
 */
class Stock
{
	CONST STATUS = [
		'1'     => 'Available',
		'0'     => 'Out of stock'
	];

    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * Status
	 *
	 * @ORM\Column(type="string", length=255, nullable=true)
	 */
	protected $status;

	/**
	 * Branch
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Branch", inversedBy="stocks", cascade={"persist"})
	 * @ORM\JoinColumn(name="branch_id", referencedColumnName="id", onDelete="SET NULL")
	 * @var Branch|null
	 */
	protected $branch;

	/**
	 * Product
	 *
	 * @ORM\ManyToOne(targetEntity="App\Entity\Product", inversedBy="stocks", cascade={"persist"})
	 * @ORM\JoinColumn(name="product_id", referencedColumnName="id", onDelete="SET NULL")
	 * @var Product|null
	 */
	protected $product;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Amount", mappedBy="stock", cascade={"persist", "remove"})
	 */
	protected $amounts;

	protected $total;
	protected $amount;

	/**
	 * Creation date
	 *
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	public function __construct()
	{
		$this->amounts = new ArrayCollection();
		$this->created = new \DateTime();
	}

    /**
     * Set id
     *
     * @param integer $id
     * @return Stock
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

	/**
	 * @return Branch|null
	 */
	public function getBranch(): ?Branch
	{
		return $this->branch;
	}

	/**
	 * @param Branch|null $branch
	 */
	public function setBranch(?Branch $branch): void
	{
		$this->branch = $branch;
	}

	/**
	 * @return Product|null
	 */
	public function getProduct(): ?Product
	{
		return $this->product;
	}

	/**
	 * @param Product|null $product
	 */
	public function setProduct(?Product $product): void
	{
		$this->product = $product;
	}

	/**
	 * @return mixed
	 */
	public function getAmounts()
	{
		return $this->amounts;
	}

	/**
	 * @param mixed $amounts
	 */
	public function setAmounts($amounts)
	{
		$this->amounts = $amounts;
	}

	public function addAmount(Amount $amount)
	{
		$amount->setStock($this);
		$this->amounts->add($amount);

		return $this;
	}

	public function removeAmount(Amount $amount)
	{
		$amount->setStock(null);
		$this->amounts->removeElement($amount);
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created): void
	{
		$this->created = $created;
	}

	/**
	 * @return mixed
	 */
	public function getTotal()
	{
		return $this->total;
	}

	/**
	 * @param mixed $total
	 */
	public function setTotal($total): void
	{
		$this->total = $total;
	}

	/**
	 * @return mixed
	 */
	public function getAmount()
	{
		return $this->amount;
	}

	/**
	 * @param mixed $amount
	 */
	public function setAmount($amount): void
	{
		$this->amount = $amount;
	}
}
