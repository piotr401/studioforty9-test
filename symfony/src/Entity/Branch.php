<?php

namespace App\Entity;

use Doctrine\Common\Collections\ArrayCollection;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity(repositoryClass="App\Repository\BranchRepository")
 * @UniqueEntity("name")
 * @ORM\HasLifecycleCallbacks
 */
class Branch
{
    /**
     * Id
     *
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

	/**
	 * Sku
	 *
	 * @ORM\Column(type="string", length=255, nullable=false, unique=true)
	 */
	protected $name;

	/**
	 * @ORM\OneToMany(targetEntity="App\Entity\Stock", mappedBy="branch", cascade={"persist", "remove"})
	 */
	protected $stocks;

	/**
	 * Creation date
	 *
	 * @var \DateTime $created
	 *
	 * @ORM\Column(type="datetime")
	 */
	protected $created;

	public function __construct()
	{
		$this->stocks = new ArrayCollection();
		$this->created = new \DateTime();
	}

    /**
     * Set id
     *
     * @param integer $id
     * @return Branch
     */
    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

	/**
	 * @return mixed
	 */
	public function getName()
	{
		return $this->name;
	}

	/**
	 * @param mixed $name
	 */
	public function setName($name): void
	{
		$this->name = $name;
	}

	/**
	 * @return mixed
	 */
	public function getStocks()
	{
		return $this->stocks;
	}

	/**
	 * @param mixed $stocks
	 */
	public function setStocks($stocks)
	{
		$this->stocks = $stocks;
	}

	public function addStock(Stock $stock)
	{
		$stock->setBranch($this);
		$this->stocks->add($stock);

		return $this;
	}

	public function removeStock(Stock $stock)
	{
		$stock->setBranch(null);
		$this->stocks->removeElement($stock);
	}

	/**
	 * @return \DateTime
	 */
	public function getCreated(): \DateTime
	{
		return $this->created;
	}

	/**
	 * @param \DateTime $created
	 */
	public function setCreated(\DateTime $created): void
	{
		$this->created = $created;
	}
}
