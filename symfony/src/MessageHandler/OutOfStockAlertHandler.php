<?php

namespace App\MessageHandler;

use App\Message\OutOfStockAlert;
use App\Utils\Managers\StockManager;
use App\Utils\Services\MailerService;
use Symfony\Component\Messenger\Attribute\AsMessageHandler;

#[AsMessageHandler]
class OutOfStockAlertHandler
{
	private $stockManager;
	private $mailerService;

	public function __construct(MailerService $mailerService, StockManager $stockManager)
	{
		$this->stockManager = $stockManager;
		$this->mailerService = $mailerService;
	}

	public function __invoke(OutOfStockAlert $alert)
	{
		$stock = $this->stockManager->findOneToEdit($alert->getStockId());

		$this->mailerService->outOfStockAlertEmail($stock);
	}
}
