<?php

namespace App\Controller;

use App\Entity\Branch;
use App\Form\Type\BranchType;
use App\Utils\Managers\BranchManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class BranchController
 *
 * @Route("/branch")
 * @package App\Controller
 */
class BranchController extends AbstractController
{
    /**
     * @Route("/", name="branch_list")
     */
    public function index(Request $request, PaginatorInterface $paginator, BranchManager $branchManager): Response
    {
		$query = $branchManager->findAllQuery();

	    $branchs = $paginator->paginate(
		    $query,
		    $request->query->getInt('page', 1),
		    30
	    );

        return $this->render('@Views/Branch/index.html.twig', [
	        'branchs' => $branchs
        ]);
    }

	/**
	 * @Route("/create", name="branch_create")
	 */
	public function create(Request $request, BranchManager $branchManager): Response
	{
		$branch = new Branch();

		$form = $this->createForm(BranchType::class, $branch);

		if ('POST' == $request->getMethod()) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {

				$branchManager->save($branch);

				return $this->redirect($this->generateUrl('branch_edit', array(
					'id' => $branch->getId()
				)));
			}
		}

		return $this->render('@Views/Branch/create.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/edit/{id}", name="branch_edit")
	 */
	public function edit(Request $request, BranchManager $branchManager, $id): Response
	{
		$branch = $branchManager->findOneToEdit($id);
		if (!$branch) {
			throw $this->createNotFoundException('Branch not found');
		}

		$form = $this->createForm(BranchType::class, $branch);

		if ('POST' == $request->getMethod()) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {

				$branchManager->save($branch);

				return $this->redirect($request->headers->get('referer'));
			}
		}

		return $this->render('@Views/Branch/edit.html.twig', [
			'form' => $form->createView(),
			'branch' => $branch
		]);
	}

	/**
	 * @Route("/delete/{id}", name="branch_delete")
	 */
	public function delete(Request $request, BranchManager $branchManager, $id): Response
	{
		$branch = $branchManager->findOneToEdit($id);
		if (!$branch) {
			throw $this->createNotFoundException('Branch not found');
		}

		$branchManager->remove($branch);

		return $this->redirect($request->headers->get('referer'));
	}
}
