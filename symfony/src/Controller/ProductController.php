<?php

namespace App\Controller;

use App\Entity\Amount;
use App\Entity\Product;
use App\Form\Type\ProductType;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\StockManager;
use Knp\Component\Pager\PaginatorInterface;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

/**
 * Class ProductController
 *
 * @Route("/product")
 * @package App\Controller
 */
class ProductController extends AbstractController
{
    /**
     * @Route("/", name="product_list")
     */
    public function index(Request $request, PaginatorInterface $paginator, ProductManager $productManager): Response
    {
		$query = $productManager->findAllQuery();

	    $products = $paginator->paginate(
		    $query,
		    $request->query->getInt('page', 1),
		    30
	    );

        return $this->render('@Views/Product/index.html.twig', [
	        'products' => $products
        ]);
    }

	/**
	 * @Route("/create", name="product_create")
	 */
	public function create(Request $request, ProductManager $productManager, StockManager $stockManager): Response
	{
		$product = new Product();

		$form = $this->createForm(ProductType::class, $product);

		if ('POST' == $request->getMethod()) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {

				$duplicates = $productManager->checkStockDuplicate($product);
				if (!$duplicates) {
					foreach ($product->getStocks() as $stock) {
						$stockManager->save($stock);
					}
					$productManager->save($product);
					return $this->redirect($this->generateUrl('product_edit', array(
						'id' => $product->getId()
					)));
				}

				$duplicates = implode(' - ', $duplicates);
				$this->addFlash('error', 'Stock already exist for branch : '.$duplicates);
			}
		}

		return $this->render('@Views/Product/create.html.twig', [
			'form' => $form->createView()
		]);
	}

	/**
	 * @Route("/edit/{id}", name="product_edit")
	 */
	public function edit(Request $request, ProductManager $productManager, StockManager $stockManager, $id): Response
	{
		$product = $productManager->findOneToEdit($id);
		if (!$product) {
			throw $this->createNotFoundException('Product not found');
		}

		$form = $this->createForm(ProductType::class, $product);

		if ('POST' == $request->getMethod()) {
			$form->handleRequest($request);

			if ($form->isSubmitted() && $form->isValid()) {

				$duplicates = $productManager->checkStockDuplicate($product);
				if (!$duplicates) {
					foreach ($product->getStocks() as $stock) {
						$stockManager->save($stock);
					}
					$productManager->save($product);
					return $this->redirect($request->headers->get('referer'));
				}

				$duplicates = implode(' - ', $duplicates);
				$this->addFlash('error', 'Stock already exist for branch : '.$duplicates);
			}
		}

		return $this->render('@Views/Product/edit.html.twig', [
			'form' => $form->createView(),
			'product' => $product
		]);
	}

	/**
	 * @Route("/delete/{id}", name="product_delete")
	 */
	public function delete(Request $request, ProductManager $productManager, $id): Response
	{
		$product = $productManager->findOneToEdit($id);
		if (!$product) {
			throw $this->createNotFoundException('Product not found');
		}

		$productManager->remove($product);

		return $this->redirect($request->headers->get('referer'));
	}
}
