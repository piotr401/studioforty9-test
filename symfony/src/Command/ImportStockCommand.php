<?php

namespace App\Command;

use App\Entity\Amount;
use App\Entity\Branch;
use App\Entity\Product;
use App\Entity\Stock;
use App\Utils\Managers\BranchManager;
use App\Utils\Managers\ProductManager;
use App\Utils\Managers\StockManager;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Helper\ProgressBar;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\HttpKernel\KernelInterface;

class ImportStockCommand extends Command
{
	private $input;
	private $output;
	private $appKernel;
	private $em;
	private $stockManager;
	private $productManager;
	private $branchManager;
	private $csvPath;

	public function __construct(KernelInterface $appKernel, EntityManagerInterface $em, StockManager $stockManager, ProductManager $productManager, BranchManager $branchManager, $csvPath)
	{
		$this->appKernel = $appKernel;
		$this->em = $em;
		$this->stockManager = $stockManager;
		$this->productManager = $productManager;
		$this->branchManager = $branchManager;
		$this->csvPath = $csvPath;
		parent::__construct();
	}

	protected static $defaultName = 'app:import:stocks';

	protected function configure()
	{
		$this->setDescription('Import stocks from CSV file');
	}

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 */
	protected function execute(InputInterface $input, OutputInterface $output)
	{
		$this->input = $input;
		$this->output = $output;

		if (!$this->import($input, $output)) {
			return Command::FAILURE;
		}

		return Command::SUCCESS;
	}

	protected function import(InputInterface $input, OutputInterface $output)
	{
		$directoryPath = $this->appKernel->getProjectDir() . $this->csvPath;

		$output->writeln('Check if the path given in .env is a directory');
		if (!is_dir($directoryPath)) {
			return null;
		}

		$output->writeln('Get file names from the directory');
		$files = scandir($directoryPath);
		unset($files[0]);
		unset($files[1]);

		$output->writeln('Get Data from each files');

		foreach ($files as $file) {

			$filePath = $directoryPath . '/' . $file;

			$datas = $this->getCsv($filePath, ',');

			$output->writeln('Start data import from '.$file);

			$progress = new ProgressBar($this->output, count($datas));
			$progress->setFormat('very_verbose');
			$progress->start();

			$i = 0;
			foreach ($datas as $row) {

				if (empty($row[0]) || empty($row[1]) || empty($row[2]) || !is_numeric($row[2])) {
					continue;
				}

				$product = $this->productManager->findOneBySku($row[0]);
				if (!$product) {
					$product = new Product();
					$product->setSku($row[0]);
					$this->em->persist($product);
					$this->em->flush();
				}

				$branch = $this->branchManager->findOneByName($row[1]);
				if (!$branch) {
					$branch = new Branch();
					$branch->setName($row[1]);
					$this->em->persist($branch);
					$this->em->flush();
				}

				$amount = new Amount($row[2]);

				$stock = $this->stockManager->findOneBySkuAndBranch($row[0], $row[1]) ?: new Stock();
				$stock->addAmount($amount);
				$stock->setBranch($branch);
				$stock->setProduct($product);

				$this->em->persist($stock);

				if ($i%50 == 0) {
					$this->em->flush();
				}

				$i++;

				$progress->advance();
			}
			$this->em->flush();

			$progress->finish();
			$this->output->writeln('Import finished');
		}

		return true;
	}

	protected function getCsv($filename, $delimiter)
	{
		$data = $this->convert($filename, $delimiter);

		return $data;
	}

	protected function convert($filename, $delimiter)
	{
		if (!file_exists($filename) || !is_readable($filename)) {
			return false;
		}

		$i = 0;
		$data = array();

		if (($handle = fopen($filename, 'r')) !== false) {
			while (($row = fgetcsv($handle, 2000, $delimiter)) !== false) {

				if ($i != 0) {
					$data[] = $row;
				}
				$i++;
			}

			fclose($handle);
		}
		return $data;
	}
}