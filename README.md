# Studioforty9-test



## Install project

```
git clone git@gitlab.com:piotr401/studioforty9-test.git

cd studioforty9-test

docker-compose build --no-cache

docker-compose up -d

docker exec -ti studioforty9_php bash

composer install

php bin/console doctrine:database:create --if-not-exists

php bin/console make:migration

php bin/console doctrine:migrations:migrate

php bin/console --env=test doctrine:database:create

php bin/console --env=test doctrine:schema:create
```




## Add Datas

```
php bin/console app:import:stocks

php bin/console --env=test doctrine:fixtures:load
```

## Configure Host
```
sudo nano /etc/hosts
```
then add 
```
127.0.0.1 studioforty9.jaavik.net
```


## Access project from browser


[http://studioforty9.jaavik.net](http://studioforty9.jaavik.net)

## Launch tests

```
docker exec -ti studioforty9_php bash

php bin/phpunit
```

## Configuration

Configure mailer dsn, "from" email and "to" email in .env file.
You can also change the CSV file path in .env file.