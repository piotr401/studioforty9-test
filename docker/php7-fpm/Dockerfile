FROM php:8.0.10-fpm

ARG APP_PROJECT_DIR_NAME

RUN apt-get update && apt-get install -y git
#In this place you can specify all the extensions you need.
RUN docker-php-ext-install mysqli pdo pdo_mysql

RUN apt-get install -y libzip-dev
RUN apt-get install -y zip
RUN apt-get install -y sudo
RUN apt-get install -y wget
RUN apt-get install -y curl
RUN apt-get install -y libmcrypt-dev
RUN apt-get install -y libxml2-dev libxslt1-dev
RUN apt-get install -y ocaml
RUN apt-get install -y nano

# PHP config
ADD conf/php.ini /usr/local/etc/php
ADD conf/project.pool.conf /usr/local/etc/php/pool.d/

# install mhsendmail
RUN wget -O /usr/sbin/mhsendmail  https://github.com/mailhog/mhsendmail/releases/download/v0.2.0/mhsendmail_linux_amd64
RUN chmod +x /usr/sbin/mhsendmail

# Install composer
RUN cd /var/www/ \
    && php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');" \
    && php composer-setup.php \
    && php -r "unlink('composer-setup.php');" \
    && mv composer.phar /usr/bin/composer \
    && chmod +x /usr/bin/composer

RUN usermod -u 1000 www-data
RUN chown -R www-data:www-data /var/www/

RUN echo Europe/Dublin | tee /etc/timezone && dpkg-reconfigure --frontend noninteractive tzdata

COPY bin/* /usr/local/bin/
RUN chmod +x /usr/local/bin/start

RUN echo "www-data:www-data" | chpasswd && adduser www-data sudo
RUN echo "www-data ALL=(root) NOPASSWD:ALL" > /etc/sudoers.d/www-data && chmod 0440 /etc/sudoers.d/www-data

USER www-data

RUN echo "alias ll='ls -l'" >> ~/.bashrc

CMD ["/usr/local/bin/start"]

WORKDIR /var/www/project/${APP_PROJECT_DIR_NAME}

EXPOSE 9000
